@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="level" style="width: 99%;"/>
                    <div class="level-left">
                        <p class="card-header-title">
                            <span class="title is-4">
                                Marcas de Vehículos
                            </span>
                        </p>
                    </div>
                    <div class="level-right">
                        <div class="buttons">
                            <a href="/" style="margin-left: 1rem; margin-bottom: 1rem; margin-top: 1rem;" class="button is-info">Volver</a>
                            <a href="/vehicle_models/create" style="margin-bottom: 1rem; margin-top: 1rem;"  class="button is-primary">Agregar</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content">
                <table class="table is-fullwidth is-hoverable">
                    <thead>
                        <tr class="title is-5">
                            <th>Nombre</th>
                            <th>Marca</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($models as $md)
                        <tr>
                            <td>
                                <a href="vehicle_models/{{$md->id}}">
                                    {{$md->name}}
                                </a>
                            </td>
                            <td>
                                {{$md->vehicleBrand->name}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection