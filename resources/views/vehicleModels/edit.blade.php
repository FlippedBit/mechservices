@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="level" style="width: 99%;"/>
                    <div class="level-left">
                        <p class="card-header-title">
                            <span class="title is-4">
                                Editar {{$vehicleModel->name}}
                            </span>
                        </p>
                    </div>
                    <div class="level-right">
                        <div class="buttons">
                            <a href="/vehicle_models" style="margin: 1rem;" class="button is-info">Cancelar</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content">
                <form action="/vehicle_models/{{$vehicleModel->id}}" method="POST">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}
                    <div class="field">
                        <label for="name" class="label">Nombre</label>
                        <div class="control">
                            <input required type="text" class="input" name="name" value="{{$vehicleModel->name}}">
                        </div>
                    </div>
                    <div class="field">
                        <label for="category_id" class="label">Marca</label>
                        <div class="control">
                            <select class="select" name="vehicle_brand_id">
                                @foreach ($brands as $br)
                                    <option value="{{$br->id}}" 
                                    @if ($vehicleModel->vehicleBrand->id == $br->id)
                                        selected
                                    @endif>
                                        {{$br->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="field">
                        <div class="control">
                            <button type="submit" class="button is-primary" >Aceptar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection