@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="level" style="width: 99%;"/>
                    <div class="level-left">
                        <p class="card-header-title">
                            <span class="title is-4">
                                {{$vehicleModel->name}}
                            </span>
                        </p>
                    </div>
                    <div class="level-right">
                        <div class="buttons">
                            <a href="/vehicle_models" style="margin-left: 1rem; margin-bottom: 1rem; margin-top: 1rem;" class="button is-info">Volver</a>
                            <a 
                            href="/vehicle_models/{{$vehicleModel->id}}/edit" 
                            style="margin-bottom: 1rem; margin-top: 1rem;" 
                            class="button is-warning"
                            >
                                Editar
                            </a>
                            <form action="/vehicle_models/{{$vehicleModel->id}}" method="POST">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button
                                type="submit"
                                style="margin-bottom: 1rem; margin-top: 1rem;" 
                                class="button is-danger"
                                >
                                Eliminar
                            </button>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content">
                <div class="content">
                    <dl>
                        <dt class="title is-5">Nombre</dt>
                        <dd class="subtitle">{{$vehicleModel->name}}</dd>
                        <dt class="title is-5">Marca</dt>
                        <dd class="subtitle">{{$vehicleModel->vehicleBrand->name}}</dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
@endsection