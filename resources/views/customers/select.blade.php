@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="level" style="width: 99%;"/>
                    <div class="level-left">
                        <p class="card-header-title">
                            <span class="title is-4">
                                Asignar vehículo a {{$customer->full_name}}
                            </span>
                        </p>
                    </div>
                    <div class="level-right">
                        <div class="buttons">
                            <a href="/customers" style="margin: 1rem;" class="button is-info">Cancelar</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content">
                <form action="/customers/{{$customer->id}}/assing_vehicle" method="POST">
                    {{ method_field('PATCH') }}
                    {{ csrf_field() }}
                    <div class="field">
                        <label for="category_id" class="label">Vehículos Disponibles</label>
                        <div class="control">
                            <select class="select" name="vehicle_id">
                                @foreach ($vehicles as $vh)
                                    <option value="{{$vh->id}}">
                                        {{$vh->vehicleModel->vehicleBrand->name}}
                                        {{$vh->vehicleModel->name}}
                                        {{$vh->year}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="field">
                        <div class="control">
                            <button type="submit" class="button is-primary" >Aceptar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection