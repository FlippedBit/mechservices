@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="level" style="width: 99%;"/>
                    <div class="level-left">
                        <p class="card-header-title">
                            <span class="title is-4">
                                {{$customer->code}}
                                {{$customer->full_name}}
                            </span>
                        </p>
                    </div>
                    <div class="level-right">
                        <div class="buttons">
                            <a href="/customers" style="margin-left: 1rem; margin-bottom: 1rem; margin-top: 1rem;" class="button is-info">Volver</a>
                            <a 
                            href="/customers/{{$customer->id}}/select_vehicle" 
                            style="margin-bottom: 1rem; margin-top: 1rem;" 
                            class="button is-primary"
                            >
                                Asignar vehículo
                            </a>
                            <a 
                            href="/customers/{{$customer->id}}/edit" 
                            style="margin-bottom: 1rem; margin-top: 1rem;" 
                            class="button is-warning"
                            >
                                Editar
                            </a>
                            <form action="/customers/{{$customer->id}}" method="POST">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button
                                type="submit"
                                style="margin-bottom: 1rem; margin-top: 1rem;" 
                                class="button is-danger"
                                >
                                Declarar Inactivo
                            </button>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content">
                <div class="content">
                    <dl>
                        <dt class="title is-5">Nombre</dt>
                        <dd class="subtitle">{{$customer->full_name}}</dd>
                        <dt class="title is-5">Dirección</dt>
                        <dd class="subtitle">{{$customer->address}}</dd>
                        <dt class="title is-5">NIT</dt>
                        <dd class="subtitle">{{$customer->nit}}</dd>
                        <dt class="title is-5">Teléfono</dt>
                        <dd class="subtitle">{{$customer->phone}}</dd>
                        <dt class="title is-5">Estado</dt>
                        <dd class="subtitle">
                            @if ($customer->is_active == 1)
                            Activo
                            @else
                            Inactivo
                            @endif
                        </dd>
                        @if ($customer->vehicles != null)
                            @for ($i = 0; $i < count($customer->vehicles); $i++)
                                <dt class="title is-5">Vehículo {{$i + 1}}</dt>
                                <dd class="subtitle">
                                    <a href="/vehicles/{{$customer->vehicles[$i]->id}}">
                                        {{$customer->vehicles[$i]->vehicleModel->vehicleBrand->name}}
                                        {{$customer->vehicles[$i]->vehicleModel->name}}
                                        {{$customer->vehicles[$i]->year}}
                                    </a>
                                </dd>
                            @endfor
                        @endif
                    </dl>
                </div>
            </div>
        </div>
    </div>
@endsection