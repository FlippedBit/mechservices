@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="level" style="width: 99%;"/>
                    <div class="level-left">
                        <p class="card-header-title">
                            <span class="title is-4">
                                Editar {{$customer->full_name}}
                            </span>
                        </p>
                    </div>
                    <div class="level-right">
                        <div class="buttons">
                            <a href="/customers" style="margin: 1rem;" class="button is-info">Cancelar</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content">
                <form action="/customers/{{$customer->id}}" method="POST">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}
                    <div class="field">
                        <label for="full_name" class="label">Nombre Completo</label>
                        <div class="control">
                            <input required type="text" class="input" name="full_name" value="{{$customer->full_name}}">
                        </div>
                    </div>
                    <div class="field">
                        <label for="address" class="label">Dirección</label>
                        <div class="control">
                            <input required type="text" class="input" name="address" value="{{$customer->address}}">
                        </div>
                    </div>
                    <div class="field">
                        <label for="full_name" class="label">Teléfono</label>
                        <div class="control">
                            <input required type="text" class="input" name="phone" value="{{$customer->phone}}">
                        </div>
                    </div>
                    <div class="field">
                        <div class="control">
                            <button type="submit" class="button is-primary" >Aceptar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection