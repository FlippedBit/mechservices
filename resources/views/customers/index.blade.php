@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="level" style="width: 99%;"/>
                    <div class="level-left">
                        <p class="card-header-title">
                            <span class="title is-4">
                                Clientes
                            </span>
                        </p>
                    </div>
                    <div class="level-right">
                        <div class="buttons">
                            <a href="/" style="margin-left: 1rem; margin-bottom: 1rem; margin-top: 1rem;" class="button is-info">Volver</a>
                            <a href="/customers/create" style="margin-bottom: 1rem; margin-top: 1rem;"  class="button is-primary">Agregar</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content">
                <table class="table is-fullwidth is-hoverable">
                    <thead>
                        <tr class="title is-5">
                            <th>Código</th>
                            <th>Nombre Completo</th>
                            <th>Dirección</th>
                            <th>NIT</th>
                            <th>Teléfono</th>
                            <th>Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($customers as $cus)
                        <tr>
                            <td>
                                <a href="customers/{{$cus->id}}">
                                    {{$cus->code}}
                                </a>
                            </td>
                            <td>{{$cus->full_name}}</td>
                            <td>{{$cus->address}}</td>
                            <td>{{$cus->nit}}</td>
                            <td>{{$cus->phone}}</td>
                            <td>
                                @if ($cus->is_active == 1)
                                    Activo
                                @else
                                    Inactivo
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection