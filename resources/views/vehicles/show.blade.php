@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="level" style="width: 99%;"/>
                    <div class="level-left">
                        <p class="card-header-title">
                            <span class="title is-4">
                                {{$vehicle->vehicleModel->vehicleBrand->name}}
                                {{$vehicle->vehicleModel->name}}
                                {{$vehicle->year}}
                            </span>
                        </p>
                    </div>
                    <div class="level-right">
                        <div class="buttons">
                            <a href="/vehicles" style="margin-left: 1rem; margin-bottom: 1rem; margin-top: 1rem;" class="button is-info">Volver</a>
                            <a 
                            href="/vehicles/{{$vehicle->id}}/edit" 
                            style="margin-bottom: 1rem; margin-top: 1rem;" 
                            class="button is-warning"
                            >
                                Editar
                            </a>
                            <form action="/vehicles/{{$vehicle->id}}" method="POST">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button
                                type="submit"
                                style="margin-bottom: 1rem; margin-top: 1rem;" 
                                class="button is-danger"
                                >
                                Deshabilitar
                            </button>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content">
                <div class="content">
                    <dl>
                        <dt class="title is-5">Código</dt>
                        <dd class="subtitle">{{$vehicle->code}}</dd>
                        <dt class="title is-5">Placa</dt>
                        <dd class="subtitle">{{$vehicle->plate}}</dd>
                        <dt class="title is-5">Año</dt>
                        <dd class="subtitle">{{$vehicle->year}}</dd>
                        <dt class="title is-5">Marca</dt>
                        <dd class="subtitle">{{$vehicle->vehicleModel->vehicleBrand->name}}</dd>
                        <dt class="title is-5">Modelo</dt>
                        <dd class="subtitle">{{$vehicle->vehicleModel->name}}</dd>
                        <dt class="title is-5">Estado</dt>
                        <dd class="subtitle">
                            @if ($vehicle->is_active == 1)
                                Activo
                            @else
                                Inactivo
                            @endif
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
@endsection