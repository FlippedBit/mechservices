@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="level" style="width: 99%;"/>
                    <div class="level-left">
                        <p class="card-header-title">
                            <span class="title is-4">
                                Lista de Vehículos
                            </span>
                        </p>
                    </div>
                    <div class="level-right">
                        <div class="buttons">
                            <a href="/" style="margin-left: 1rem; margin-bottom: 1rem; margin-top: 1rem;" class="button is-info">Volver</a>
                            <a href="/vehicles/create" style="margin-bottom: 1rem; margin-top: 1rem;"  class="button is-primary">Agregar</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content">
                <table class="table is-fullwidth is-hoverable">
                    <thead>
                        <tr class="title is-5">
                            <th>Código</th>
                            <th>Placa</th>
                            <th>Año</th>
                            <th>Marca</th>
                            <th>Modelo</th>
                            <th>Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($vehicles as $vh)
                        <tr>
                            <td>
                                <a href="vehicles/{{$vh->id}}">
                                    {{$vh->code}}
                                </a>
                            </td>
                            <td>
                                {{$vh->plate}}
                            </td>
                            <td>
                                {{$vh->year}}
                            </td>
                            <td>
                                {{$vh->vehicleModel->vehicleBrand->name}}
                            </td>
                            <td>
                                {{$vh->vehicleModel->name}}
                            </td>
                            <td>
                                @if ($vh->is_active == 1)
                                    Activo
                                @else
                                    Inactivo
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection