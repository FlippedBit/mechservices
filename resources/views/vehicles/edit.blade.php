@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="level" style="width: 99%;"/>
                    <div class="level-left">
                        <p class="card-header-title">
                            <span class="title is-4">
                                Editar
                                {{$vehicle->vehicleModel->vehicleBrand->name}}
                                {{$vehicle->vehicleModel->name}}
                                {{$vehicle->year}}
                            </span>
                        </p>
                    </div>
                    <div class="level-right">
                        <div class="buttons">
                            <a href="/vehicles" style="margin: 1rem;" class="button is-info">Cancelar</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content">
                <form action="/vehicles/{{$vehicle->id}}" method="POST">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}

                    <div class="field">
                        <label for="code" class="label">Código</label>
                        <div class="control">
                            <input required type="text" class="input" name="code" value="{{$vehicle->code}}">
                        </div>
                    </div>

                    <div class="field">
                        <label for="plate" class="label">Placa</label>
                        <div class="control">
                            <input required type="text" class="input" name="plate" value="{{$vehicle->plate}}">
                        </div>
                    </div>

                    <div class="field">
                        <label for="year" class="label">Año</label>
                        <div class="control">
                            <input required type="text" class="input" name="year" value="{{$vehicle->year}}">
                        </div>
                    </div>

                    <div class="field">
                        <label for="category_id" class="label">Modelo</label>
                        <div class="control">
                            <select class="select" name="vehicle_model_id">
                                @foreach ($models as $md)
                                    <option value="{{$md->id}}" 
                                    @if ($vehicle->vehicleModel->id == $md->id)
                                        selected
                                    @endif>
                                        {{$md->vehicleBrand->name}}
                                        {{$md->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="field">
                        <div class="control">
                            <button type="submit" class="button is-primary" >Aceptar</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection