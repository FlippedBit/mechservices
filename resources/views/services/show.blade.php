@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="level" style="width: 99%;"/>
                    <div class="level-left">
                        <p class="card-header-title">
                            <span class="title is-4">
                                {{$service->name}}
                            </span>
                        </p>
                    </div>
                    <div class="level-right">
                        <div class="buttons">
                            <a href="/services" style="margin-left: 1rem; margin-bottom: 1rem; margin-top: 1rem;" class="button is-info">Volver</a>
                            <a 
                            href="/services/{{$service->id}}/edit" 
                            style="margin-bottom: 1rem; margin-top: 1rem;" 
                            class="button is-warning"
                            >
                                Editar
                            </a>
                            <form action="/services/{{$service->id}}" method="POST">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button
                                type="submit"
                                style="margin-bottom: 1rem; margin-top: 1rem;" 
                                class="button is-danger"
                                >
                                Eliminar
                            </button>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content">
                <div class="content">
                    <dl>
                        <dt class="title is-4">Nombre</dt>
                        <dd class="subtitle">{{$service->name}}</dd>
                        <dt class="title is-4">Descripción</dt>
                        <dd class="subtitle">{{$service->description}}</dd>
                        <dt class="title is-4">Precio</dt>
                        <dd class="subtitle">Q.{{$service->price}}</dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
@endsection