@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="level" style="width: 99%;"/>
                    <div class="level-left">
                        <p class="card-header-title">
                            <span class="title is-4">
                                Servicios
                            </span>
                        </p>
                    </div>
                    <div class="level-right">
                        <div class="buttons">
                            <a href="/" style="margin-left: 1rem; margin-bottom: 1rem; margin-top: 1rem;" class="button is-info">Volver</a>
                            <a href="/services/create" style="margin-bottom: 1rem; margin-top: 1rem;"  class="button is-primary">Agregar</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content">
                <table class="table is-fullwidth is-hoverable">
                    <thead>
                        <tr class="title is-5">
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Precio</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($services as $srv)
                        <tr>
                            <td>
                                <a href="services/{{$srv->id}}">
                                    {{$srv->name}}
                                </a>
                            </td>
                            <td>{{$srv->description}}</td>
                            <td style="text-align: right">Q.{{$srv->price}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection