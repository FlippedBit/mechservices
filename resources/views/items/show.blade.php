@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="level" style="width: 99%;"/>
                    <div class="level-left">
                        <p class="card-header-title">
                            <span class="title is-4">
                                {{$item->name}}
                            </span>
                        </p>
                    </div>
                    <div class="level-right">
                        <div class="buttons">
                            <a href="/items" style="margin-left: 1rem; margin-bottom: 1rem; margin-top: 1rem;" class="button is-info">Volver</a>
                            <a 
                            href="/items/{{$item->id}}/edit" 
                            style="margin-bottom: 1rem; margin-top: 1rem;" 
                            class="button is-warning"
                            >
                                Editar
                            </a>
                            <form action="/items/{{$item->id}}" method="POST">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button
                                type="submit"
                                style="margin-bottom: 1rem; margin-top: 1rem;" 
                                class="button is-danger"
                                >
                                Marcar no disponible
                            </button>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content">
                <div class="content">
                    <dl>
                        <dt class="title is-5">Código</dt>
                        <dd class="subtitle">{{$item->code}}</dd>
                        <dt class="title is-5">Nombre</dt>
                        <dd class="subtitle">{{$item->name}}</dd>
                        <dt class="title is-5">Descripción</dt>
                        <dd class="subtitle">{{$item->description}}</dd>
                        <dt class="title is-5">Precio min.</dt>
                        <dd class="subtitle">Q.{{$item->min_price}}</dd>
                        <dt class="title is-5">Precio</dt>
                        <dd class="subtitle">Q.{{$item->price}}</dd>
                        <dt class="title is-5">Disponibilidad</dt>
                        <dd class="subtitle">
                            @if ($item->is_active == 1)
                            Disponible
                            @else
                            No Disponible
                            @endif
                        </dd>
                        <dt class="title is-5">Categoría</dt>
                        <dd class="subtitle">{{$item->category->name}}</dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
@endsection