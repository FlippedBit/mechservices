@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="level" style="width: 99%;"/>
                    <div class="level-left">
                        <p class="card-header-title">
                            <span class="title is-4">
                                Editar {{$item->name}}
                            </span>
                        </p>
                    </div>
                    <div class="level-right">
                        <div class="buttons">
                            <a href="/items" style="margin: 1rem;" class="button is-info">Cancelar</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content">
                <form action="/items/{{$item->id}}" method="POST">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}
                    <div class="field">
                        <label for="name" class="label">Nombre</label>
                        <div class="control">
                            <input required type="text" class="input" name="name" value="{{$item->name}}">
                        </div>
                    </div>
                    <div class="field">
                        <label for="description" class="label">Descripción</label>
                        <div class="control">
                            <textarea required type="text" class="textarea" rows="4" name="description" style="resize: none">
{{$item->description}}
                                </textarea>
                            </div>
                        </div>
                        <div class="field">
                            <label for="min_price" class="label">Precio Min.</label>
                            <div class="control">
                                <input required type="text" class="input" name="min_price" value="{{$item->min_price}}">
                            </div>
                        </div>
                        <div class="field">
                            <label for="min_price" class="label">Precio</label>
                            <div class="control">
                                <input required type="text" class="input" name="min_price" value="{{$item->price}}">
                            </div>
                        </div>

                        <div class="field">
                        <label for="category_id" class="label">Categoría</label>
                        <div class="control">
                            <select class="select" name="category_id">
                                @foreach ($categories as $cat)
                                    <option value="{{$cat->id}}"
                                        @if ($item->category->id == $cat->id)
                                            selected
                                        @endif
                                        >
                                        {{$cat->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="field">
                        <div class="control">
                            <button type="submit" class="button is-primary" >Aceptar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection