@extends('layouts.app')
@section('content')
<style>
    .center{
        height: 110vh;
    }
</style>
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="level" style="width: 99%;"/>
                    <div class="level-left">
                        <p class="card-header-title">
                            <span class="title is-4">
                                Agregar un nuevo artículo
                            </span>
                        </p>
                    </div>
                    <div class="level-right">
                        <div class="buttons">
                            <a href="/items" style="margin: 1rem;" class="button is-info">Cancelar</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content">
                
                <form action="/items/" method="POST">
                    {{ csrf_field() }}
                    
                    <div class="field">
                        <label for="code" class="label">Código</label>
                        <div class="control">
                            <input required type="text" class="input" name="code">
                        </div>
                    </div>

                    <div class="field">
                        <label for="name" class="label">Nombre</label>
                        <div class="control">
                            <input required type="text" class="input" name="name">
                        </div>
                    </div>

                    <div class="field">
                        <label for="description" class="label">Descripción</label>
                        <div class="control">
                            <textarea required type="text" class="textarea" rows="4" name="description" style="resize: none"></textarea>
                        </div>
                    </div>
                    <div class="field">
                        <label for="min_price" class="label">Precio Min.</label>
                        <div class="control">
                            <input required type="text" class="input" name="min_price"
                            pattern="^\d*(\.\d{0,2})?$">
                        </div>
                    </div>

                    <div class="field">
                        <label for="price" class="label">Precio</label>
                        <div class="control">
                            <input required type="text" class="input" name="price"
                            pattern="^\d*(\.\d{0,2})?$">
                        </div>
                    </div>

                    <div class="field">
                        <label for="category_id" class="label">Categoría</label>
                        <div class="control">
                            <select class="select" name="category_id">
                                @foreach ($categories as $cat)
                                    <option value="{{$cat->id}}">
                                        {{$cat->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="field">
                        <div class="control">
                            <button type="submit" class="button is-primary" >Aceptar</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection