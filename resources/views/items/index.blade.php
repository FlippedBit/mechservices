@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="level" style="width: 99%;"/>
                    <div class="level-left">
                        <p class="card-header-title">
                            <span class="title is-4">
                                Artículos
                            </span>
                        </p>
                    </div>
                    <div class="level-right">
                        <div class="buttons">
                            <a href="/" style="margin-left: 1rem; margin-bottom: 1rem; margin-top: 1rem;" class="button is-info">Volver</a>
                            <a href="/items/create" style="margin-bottom: 1rem; margin-top: 1rem;" class="button is-primary">Agregar</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content">
                <table class="table is-fullwidth is-hoverable">
                    <thead>
                        <tr class="title is-5">
                            <th>Código</th>
                            <th>Nombre</th>
                            <th>DescripciÃ³n</th>
                            <th style="text-align: right">Precio Min.</th>
                            <th style="text-align: right">Precio</th>
                            <th>Disponibilidad</th>
                            <th>Categoría</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $it)
                        <tr>
                            <td>
                                <a href="items/{{$it->id}}">
                                    {{$it->code}}
                                </a>
                            </td>
                            <td>{{$it->name}}</td>
                            <td>{{$it->description}}</td>
                            <td style="text-align: right">Q.{{$it->min_price}}</td>
                            <td style="text-align: right">Q.{{$it->price}}</td>
                            <td>
                                @if ($it->is_active == 1)
                                    Disponible
                                @else
                                    No Disponible
                                @endif
                            </td>
                            <td>{{$it->category->name}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection