@extends('layouts.app')

@section('content')
<div class="container">
            <div class="card">
                <div class="card-header">
                    <span class="card-header-title">
                        <p class="title is-4">
                            Dashboard
                        </p>
                    </span>
                </div>
                <div class="card-content">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <p class="title is-5">
                        Seleccione una opción
                    </p>
                    <div class="columns">
                        <div class="column">
                        <article class="panel is-info">
                              <p class="panel-heading">
                                Servicios
                              </p>
                              <a href="services" class="panel-block">
                                Lista de servicios
                              </a>
                        </article>
                        </div>
                        <div class="column">
                        <article class="panel is-dark">
                              <p class="panel-heading">
                                Clientes
                              </p>
                              <a href="customers" class="panel-block is-active">
                                Lista de clientes
                              </a>
                        </article>
                        </div>
                        <div class="column">
                        <article class="panel is-warning">
                            <p class="panel-heading">
                                Artículos
                              </p>
                              <a href="items" class="panel-block">
                                  Lista de artículos
                                </a>
                                <a href="categories" class="panel-block">
                                  Lista de categorías
                                </a>
                        </article>
                        </div>
                        <div class="column">
                        <article class="panel is-success">
                              <p class="panel-heading">
                                Vehículos
                              </p>
                              <a href="vehicles" class="panel-block">
                                Lista de vehículos
                              </a>
                              <a href="vehicle_brands" class="panel-block">
                                Lista de marcas
                              </a>
                              <a href="vehicle_models" class="panel-block">
                                Lista de modelos
                              </a>
                        </article>
                        </div>
                    </div>
                </div>
            </div>
</div>
@endsection
