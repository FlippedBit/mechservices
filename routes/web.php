<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::resource('services', 'ServiceController');
Route::resource('categories', 'CategoryController');
Route::resource('items', 'ItemController');
Route::resource('vehicle_brands', 'VehicleBrandController');
Route::resource('vehicle_models', 'VehicleModelController');
Route::resource('vehicles', 'VehicleController');
Route::resource('customers', 'CustomerController');

Route::get('customers/{customer}/select_vehicle', 'CustomerController@select');
Route::patch('customers/{customer}/assing_vehicle', 'CustomerController@assign');
