<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('items', 'ApiController@getItems');
Route::get('vehicles', 'ApiController@getVehicles');
Route::get('customers', 'ApiController@getCustomers');
Route::get('services', 'ApiController@getServices');

Route::post('services', 'ApiController@addOrder');
Route::patch('services', 'ApiController@updateOrderStatus');
