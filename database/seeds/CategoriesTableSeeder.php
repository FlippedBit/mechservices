<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Consumibles',
                'description' => 'suplementos consumibles'
            ],
            [
                'name' => 'Repuestos',
                'description' => 'parter nuevas para reparaciones'
            ]
        ];
        DB::table('categories')->insert($data);
    }
}
