<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VehicleBrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'honda'],
            ['name' => 'suzuki']
        ];
        DB::table('vehicle_brands')->insert($data);
    }
}
