<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Armando Ruedas',
                'email' => 'arm@ruedas.com',
                'password' => bcrypt('arm123')
            ],
        ];
        DB::table('users')->insert($data);
    }
}
