<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(ServicesTableSeeder::class);
         $this->call(CategoriesTableSeeder::class);
         $this->call(ItemsTableSeeder::class);
         $this->call(VehicleBrandsTableSeeder::class);
         $this->call(VehicleModelsTableSeeder::class);
         $this->call(VehicleTableSeeder::class);
         $this->call(CustomersTableSeeder::class);
    }
}
