<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'code' => 'a1',
                'name' => 'aceite',
                'description' => 'aceite para motos',
                'min_price' => 55.00,
                'price' => 60.00,
                'category_id' => '1'
            ],
            [
                'code' => 'b1',
                'name' => 'fricciones',
                'description' => 'fricciones para moto',
                'min_price' => 75.00,
                'price' => 65.00,
                'category_id' => '2'
            ],
        ];
        DB::table('items')->insert($data);
    }
}
