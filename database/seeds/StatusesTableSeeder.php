<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['id' => 1, 'name' => 'ABIERTA'], //
            ['id' => 2, 'name' => 'ARCHIVADA'], // abierta pero pasa
            // a este estado despues de 30 dÃ­as 
            ['id' => 3, 'name' => 'FINALIZADA'],
            ['id' => 4, 'name' => 'ANULADA'],
        ];
        DB::table('statuses')->insert($data);
    }
}
