<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'code' => 'OR001',

            'vehicle' => json_encode(
                [
                    'code' => 'V18001',
                    'brand' => 'SUZUKI',
                    'model' => 'AN 125',
                    'year' => '2011',
                    'plate' => 'M-291DFG',
                ]
            ),

            'customer' => json_encode(
                [
                    'code' => 'C001',
                    'full_name' => 'Steve García',
                    'nit' => '481994-1',
                    'address' => 'ciudad',
                    'phone' => '5555-5555',
                ]
            ),

            'master_detail' => json_encode(
                [
                    'repuestos' => [
                        [
                            'código' => 'R001',
                            'descripción' => 'Aceite castrol 1L',
                            'cantidad' => 1,
                            'unidad' => 65.00,
                            'subtotal' => 65.00,
                        ],
                        [
                            'código' => 'R025',
                            'descripción' => 'Filtro aceite cartucho',
                            'cantidad' => 1,
                            'unidad' => 10.00,
                            'subtotal' => 10.00,
                        ],
                    ],
                    'servicios' => [
                        [
                            'cantidad' => 1,
                            'descripción' => 'Cambio aceite motocicleta 1cil.',
                            'unidad' => 20.00,
                            'subtotal' => 20.00,
                        ],
                        [
                            'cantidad' => 1,
                            'descripción' => 'limpieza filtro de aire',
                            'unidad' => 10.00,
                            'subtotal' => 10.00,
                        ],

                    ],
                    'materiales' => [
                        [
                            'cantidad' => 1,
                            'descripción' => 'arandela bronce',
                            'unidad' => 3.00,
                            'subtotal' => 3,
                        ],
                    ],
                    'otros' => [],
                ]
            ),

            'totals' => json_encode(
                [
                    'repuestos' => 75.00,
                    'servicios' => 30.00,
                    'materiales' => 3.00,
                    'otros' => 0.00,
                    'total' => 108.00,
                ]
            ),

            'meta' => json_encode(
                [
                    'worked_by' => 'Juan Martínez',
                    'customer_observations' => [
                        'revisar fuga de motor',
                    ],
                    'maintenance_suggestions' => [
                        'se necesita dearmar el motor para reparar la fuga',
                    ],
                ]
            ),

            'status_id' => 1,

        ];
        DB::table('orders')->insert($data);
    }
}
