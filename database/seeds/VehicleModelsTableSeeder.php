<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VehicleModelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'samurai',
                'vehicle_brand_id' => 2
            ],
            [
                'name' => 'crv',
                'vehicle_brand_id' => 1
            ]
        ];
        
        DB::table('vehicle_models')->insert($data);
    }
}
