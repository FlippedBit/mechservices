<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'code' => 'C001',
                'full_name' => 'John Doe',
                'address' => 'P. Sherman, Sidney',
                'nit' => '123456789',
                'phone' => '12345678',
            ],
            [
                'code' => 'C002',
                'full_name' => 'Jane Doe',
                'address' => 'Dope St. Coolsville',
                'nit' => '987654321',
                'phone' => '87654321',
            ]
        ];
        DB::table('customers')->insert($data);
    }
}
