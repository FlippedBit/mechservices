<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VehicleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'code' => 'v01',
                'plate' => 'C001',
                'year' => '2001',
                'vehicle_model_id' => 1,
            ],
            [
                'code' => 'v02',
                'plate' => 'C002',
                'year' => '1998',
                'vehicle_model_id' => 2,
            ],
        ];
        DB::table('vehicles')->insert($data);
    }
}
