<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Limpieza filtro',
                'description' => 'something something cilinder',
                'price' => 20.00
            ],
            [
                'name' => 'Cambio aceite',
                'description' => 'something something oil',
                'price' => 60.00
            ],
            [
                'name' => 'Cambio líquido frenos',
                'description' => 'something something switch',
                'price' => 40.00
            ]
        ];
        DB::table('services')->insert($data);
    }
}
