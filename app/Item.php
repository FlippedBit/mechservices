<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = ['code', 'name', 'description', 'min_price', 'price', 'is_active', 'category_id'];

    public function category(){
        return $this->belongsTo('App\Category');
    }
}
