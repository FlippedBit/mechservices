<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleModel extends Model
{
    protected $fillable = ['name', 'vehicle_brand_id'];

    public function vehicleBrand()
    {
        return $this->belongsTo('App\VehicleBrand');
    }
    public function vehicle(){
        return $this->hasMany('App\Vehicle');
    }
}
