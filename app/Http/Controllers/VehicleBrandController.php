<?php

namespace App\Http\Controllers;

use App\VehicleBrand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VehicleBrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user != null) {
            $brands = VehicleBrand::all();
            return view('vehicleBrands.index')->withBrands($brands);
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if ($user != null) {
            return view('vehicleBrands.create');
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        if ($user != null) {
            $attributes = request()->validate([
                'name' => ['required', 'min:3'],
            ]);
            $brand = VehicleBrand::create($attributes);
            return redirect('/vehicle_brands/' . $brand['id']);
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VehicleBrand  $vehicleBrand
     * @return \Illuminate\Http\Response
     */
    public function show(VehicleBrand $vehicleBrand)
    {
        $user = Auth::user();
        if ($user != null) {
            return view('vehicleBrands.show', compact('vehicleBrand'));
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VehicleBrand  $vehicleBrand
     * @return \Illuminate\Http\Response
     */
    public function edit(VehicleBrand $vehicleBrand)
    {
        $user = Auth::user();
        if ($user != null) {
            return view('vehicleBrands.edit', compact('vehicleBrand'));
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VehicleBrand  $vehicleBrand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VehicleBrand $vehicleBrand)
    {
        $user = Auth::user();
        if ($user != null) {
            $vehicleBrand->update(request()->all());
            return redirect("/vehicle_brands" . "/" . $vehicleBrand->id);
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VehicleBrand  $vehicleBrand
     * @return \Illuminate\Http\Response
     */
    public function destroy(VehicleBrand $vehicleBrand)
    {
        $user = Auth::user();
        if ($user != null) {
            $vehicleBrand->delete();
            return redirect('vehicle_brands/');
        } else {
            abort(403, 'No active session');
        }
    }
}
