<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user != null) {
            $services = Service::all();
            return view('services.index')->withServices($services);
        }else {
            abort(403, 'No active session');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if ($user != null) {
            return view('services.create');
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $user = Auth::user();
        if ($user != null) {
            $attributes = request()->validate([
                'name' => ['required', 'min:5'],
                'description' => ['required', 'min:5'],
                'price' => ['required']
            ]);
            $service = Service::create($attributes);
            return redirect('/services/' . $service['id']);
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        $user = Auth::user();
        if ($user != null) {
            return view('services.show', compact('service'));
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        $user = Auth::user();
        if ($user != null) {
            return view('services.edit', compact('service'));
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update( Service $service)
    {
        $user = Auth::user();
        if ($user != null) {
            $service->update(request()->all());
            return redirect("/services" . "/" . $service->id);
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        $user = Auth::user();
        if ($user != null) {
            $service->delete();
            return redirect('services/');
        } else {
            abort(403, 'No active session');
        }
    }
}
