<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user != null) {
            $categories = Category::all();
            return view('categories.index')->withCategories($categories);
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if ($user != null) {
            return view('categories.create');
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        if ($user != null) {
            $attributes = request()->validate([
                'name' => ['required', 'min:5'],
                'description' => ['required', 'min:5']
            ]);
            $category = Category::create($attributes);
            return redirect('/categories/' . $category['id']);
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $user = Auth::user();
        if ($user != null) {
            return view('categories.show', compact('category'));
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $user = Auth::user();
        if ($user != null) {
            return view('categories.edit', compact('category'));
        } else {
            abort(403, 'No active session');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $user = Auth::user();
        if ($user != null) {
            $category->update(request()->all());
            return redirect("/categories" . "/" . $category->id);
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $user = Auth::user();
        if ($user != null) {
            $category->delete();
            return redirect('categories/');
        } else {
            abort(403, 'No active session');
        }
    }
}
