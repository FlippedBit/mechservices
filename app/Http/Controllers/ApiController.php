<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Item;
use App\Vehicle;
use App\Customer;
use App\Service;

class ApiController extends Controller
{
    public function getItems(){
        $items = Item::all()->where('is_active' , 1);
        return response()->json($items);
    }

    public function getVehicles(){
        $vehicles = DB::table('vehicle_brands')
        ->join(
            'vehicle_models', 
            'vehicle_brands.id', 
            '=', 
            'vehicle_models.vehicle_brand_id'
        )->join(
            'vehicles',
            'vehicles.id', 
            '=', 
            'vehicle_models.id'
        )->where(
            'vehicles.is_active',
            '1'
        )->select(
            'vehicles.code', 
            'vehicle_models.name as model', 
            'vehicle_brands.name as brand', 
            'vehicles.year', 
            'vehicles.plate'
        )->get();
        return response()->json($vehicles);
    }

    public function getCustomers(){
        $customers = Customer::all()->where('is_active', 1);
        return response()->json($customers);
    }

    public function getServices(){
        $services = Service::all();
        return response()->json($services);
    }

    public function addOrder(){
        
    }

    public function updateOrderStatus(){
        
    }
}
