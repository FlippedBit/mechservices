<?php

namespace App\Http\Controllers;

use App\VehicleModel;
use App\VehicleBrand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VehicleModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user != null) {
            $models = VehicleModel::all();
            return view('vehicleModels.index')->withModels($models);
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if ($user != null) {
            $brands = VehicleBrand::all();
            return view('vehicleModels.create')->withBrands($brands);
        } else {
            abort(403, 'No active session');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        if ($user != null) {
            $attributes = request()->validate([
                'name' => ['required', 'min:3'],
                'vehicle_brand_id' => ['required'],
            ]);
            $model = VehicleModel::create($attributes);
            return redirect('/vehicle_models/' . $model['id']);
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VehicleModel  $vehicleModel
     * @return \Illuminate\Http\Response
     */
    public function show(VehicleModel $vehicleModel)
    {
        $user = Auth::user();
        if ($user != null) {
            return view('vehicleModels.show', compact('vehicleModel'));
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VehicleModel  $vehicleModel
     * @return \Illuminate\Http\Response
     */
    public function edit(VehicleModel $vehicleModel)
    {
        $user = Auth::user();
        if ($user != null) {
            $brands = VehicleBrand::all();
            return view('vehicleModels.edit', compact('vehicleModel', 'brands'));
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VehicleModel  $vehicleModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VehicleModel $vehicleModel)
    {
        $user = Auth::user();
        if ($user != null) {
            $vehicleModel->update(request()->all());
            return redirect("/vehicle_models" . "/" . $vehicleModel->id);
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VehicleModel  $vehicleModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(VehicleModel $vehicleModel)
    {
        $user = Auth::user();
        if ($user != null) {
            $vehicleModel->delete();
            return redirect('vehicle_models/');
        } else {
            abort(403, 'No active session');
        }
    }
}
