<?php

namespace App\Http\Controllers;

use App\Item;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user != null) {
            $items = Item::all();
            return view('items.index')->withItems($items);
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if ($user != null) {
            $categories = Category::all();
            return view('items.create')->withCategories($categories);
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $user = Auth::user();
        if ($user != null) {
            $attributes = request()->validate([
                'code' => ['required', 'min:2'],
                'name' => ['required', 'min:5'],
                'description' => ['required', 'min:5'],
                'min_price' => ['required'],
                'price' => ['required'],
                'category_id' => ['required'],
            ]);
            $item = Item::create($attributes);
             return redirect('/items/' . $item['id']);
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        $user = Auth::user();
        if ($user != null) {
            return view('items.show', compact('item'));
        } else {
            abort(403, 'No active session');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        $user = Auth::user();
        if ($user != null) {
            $categories = Category::all();
            return view('items.edit', compact('item', 'categories'));
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Item $item)
    {
        $user = Auth::user();
        if ($user != null) {
            $item->update(request()->all());
            return redirect("/items" . "/" . $item->id);
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        $user = Auth::user();
        if ($user != null) {
            $item->update(['is_active' => 0]);
            return redirect('items/');
        } else {
            abort(403, 'No active session');
        }
    }
}
