<?php

namespace App\Http\Controllers;

use App\Vehicle;
use App\VehicleModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user != null) {
            $vehicles = Vehicle::all();
            return view('vehicles.index')->withVehicles($vehicles);
        } else {
            abort(403, 'No active session');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if ($user != null) {
            $models = VehicleModel::all();
            return view('vehicles.create')->withModels($models);
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        if ($user != null) {
            $attributes = request()->validate([
                'code' => ['required'],
                'plate' => ['required'],
                'year' => ['required'],
                'vehicle_model_id' => ['required'],
            ]);
            $vehicle = Vehicle::create($attributes);
            return redirect('/vehicles/' . $vehicle['id']);
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show(Vehicle $vehicle)
    {
        $user = Auth::user();
        if ($user != null) {
            return view('vehicles.show', compact('vehicle'));
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function edit(Vehicle $vehicle)
    {
        $user = Auth::user();
        if ($user != null) {
            $models = VehicleModel::all();
            return view('vehicles.edit', compact('vehicle', 'models'));
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehicle $vehicle)
    {
        $user = Auth::user();
        if ($user != null) {
            $vehicle->update(request()->all());
            return redirect("/vehicles/" . $vehicle->id);
        } else {
            abort(403, 'No active session');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehicle $vehicle)
    {
        $user = Auth::user();
        if ($user != null) {
            $vehicle->update(['is_active' => 0]);
            return redirect('vehicles/');
        } else {
            abort(403, 'No active session');
        }
    }
}
