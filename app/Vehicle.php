<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable = ['code', 'plate', 'year', 'is_active', 'vehicle_model_id'];

    public function vehicleModel()
    {
        return $this->belongsTo('App\VehicleModel');
    }

    public function customers()
    {
        return $this->belongsToMany('App\Customer');
    }

}
