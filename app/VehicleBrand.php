<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleBrand extends Model
{
    protected $fillable = ['name'];

    public function vehicleModel()
    {
        return $this->hasMany('App\VehicleModel');
    }

}
