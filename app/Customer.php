<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['code', 'full_name', 'address', 'nit', 'phone', 'is_active'];

    public function vehicles(){
        return $this->belongsToMany('App\Vehicle');
    }
}
